Feature: Send e-mail
  As a user
  I want to log-in to mail service
  I want to write a test message
  Check that e-mail is send

  Scenario Outline: Search pictures on Google
    Given User opens '<homePage>' page
    And User enter '<login>' and '<password>' and click submit
    When User click on new message button
    And User type '<toAddress>' mail address
    And User type '<subject>' text in SUBJECT
    And User type '<messageText>' text in MESSAGE
    And User send message
    Then User checks that outbox contains mail to '<toAddress>' and '<subject>'

    Examples:
      | homePage                 | login              | password      | toAddress          | subject      | messageText       |
      | https://freemail.ukr.net | user_email@ukr.net | 123456qwerty! | user_email@ukr.net | Test subject | Test message text |
