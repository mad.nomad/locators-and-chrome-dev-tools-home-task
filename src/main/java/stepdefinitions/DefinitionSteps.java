package stepdefinitions;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;
import pages.MailPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Thread.sleep;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;

    WebDriver driver;
    HomePage homePage;
    MailPage mailPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User enter {string} and {string} and click submit")
    public void userEnterCredentialsAndClickSubmit(final String login, final String password) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isLoginFieldVisible());
        homePage.enterTextToLoginField(login);
        assertTrue(homePage.isPasswordFieldVisible());
        homePage.enterTextToPasswordField(password);
        homePage.getSubmitLoginButton().click();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @When("User click on new message button")
    public void userClickOnNewMessageButton() {
        mailPage = pageFactoryManager.getMailPage();
        mailPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, mailPage.getNewMessageButton());
        mailPage.getNewMessageButton().click();
        mailPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, mailPage.getSendMessageButton());
    }

    @And("User type {string} mail address")
    public void userFillToAddressInput(final String toAddress) {
        mailPage.setMessageToFieldInput(toAddress);
        mailPage.getMessageSubjectInput().click();
    }

    @And("User type {string} text in SUBJECT")
    public void userFillSubjectInput(final String subject) {
        mailPage.setMessageSubjectInput(subject);
    }

    @And("User type {string} text in MESSAGE")
    public void userFillMessageText(final String text) {
        driver.switchTo().frame(mailPage.getMessageTextFrame());
        mailPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, mailPage.getMessageTextInput());
        mailPage.setMessageTextInput(text);
        driver.switchTo().parentFrame();
    }

    @And("User send message")
    public void userSendMessage() {
        mailPage.getSendMessageButton().click();
    }

    @Then("User checks that outbox contains mail to {string} and {string}")
    public void userChecksThatMailIsSend(final String toAddress, final String subject) {
        mailPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, mailPage.getSendMessageStatus());
        assertTrue(mailPage.getSendMessageStatus().isDisplayed());
    }

}
