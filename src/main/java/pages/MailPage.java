package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MailPage extends BasePage {

    @FindBy(xpath = "//button[@class='button primary compose']")
    private WebElement newMessageButton;

    @FindBy(xpath = "//button[@class='button primary send']")
    private WebElement sendMessageButton;

    @FindBy(xpath = "//input[@name='toFieldInput']")
    private WebElement messageToFieldInput;

    @FindBy(xpath = "//input[@name='subject']")
    private WebElement messageSubjectInput;

    @FindBy(xpath = "//body[@id='tinymce']")
    private WebElement messageTextInput;

    @FindBy(xpath = "//a[@id='10001']")
    private WebElement outboxLink;

    @FindBy(xpath = "//a[@class='msglist__row-address-name']")
    private List<WebElement> outboxListOfAddresses;

    @FindBy(xpath = "//a[@class='msglist__row-subject']")
    private List<WebElement> outboxListOfSubjects;

    @FindBy(xpath = "//iframe[@id='mce_0_ifr']")
    private WebElement messageTextFrame;

    @FindBy(xpath = "//div[@class='sendmsg__ads-ready']")
    private WebElement messageSend;

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getNewMessageButton() {
        return newMessageButton;
    }

    public WebElement getSendMessageButton() {
        return sendMessageButton;
    }

    public WebElement getSendMessageStatus() {
        return messageSend;
    }

    public WebElement getMessageToFieldInput() {
        return messageToFieldInput;
    }

    public WebElement getMessageSubjectInput() {
        return messageSubjectInput;
    }

    public WebElement getMessageTextFrame() {
        return messageTextFrame;
    }

    public WebElement getMessageTextInput() {
        return messageTextInput;
    }

    public void setMessageToFieldInput(final String toAddress) {
        messageToFieldInput.clear();
        messageToFieldInput.sendKeys(toAddress);
    }

    public void setMessageSubjectInput(final String subject) {
        messageSubjectInput.clear();
        messageSubjectInput.sendKeys(subject);
    }

    public void setMessageTextInput(final String text) {
        messageTextInput.clear();
        messageTextInput.sendKeys(text);
    }

}
