package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement buttonSubmitLogin;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean isLoginFieldVisible() {
        return loginField.isDisplayed();
    }

    public void enterTextToLoginField(final String login) {
        loginField.clear();
        loginField.sendKeys(login);
    }

    public boolean isPasswordFieldVisible() {
        return passwordField.isDisplayed();
    }

    public void enterTextToPasswordField(final String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public WebElement getSubmitLoginButton() {
        return buttonSubmitLogin;
    }

}
